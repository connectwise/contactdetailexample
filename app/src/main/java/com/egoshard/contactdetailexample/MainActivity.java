package com.egoshard.contactdetailexample;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import static android.provider.ContactsContract.CommonDataKinds.StructuredPostal;


public class MainActivity extends Activity {

    private static final int REQUEST_SELECT_CONTACT = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * @param view
     */
    public void onSelectButtonClick(View view) {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_SELECT_CONTACT);
        }
    }

    /**
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_SELECT_CONTACT && resultCode == RESULT_OK) {

            Uri contactUri = data.getData();
            if (contactUri == null) {
                System.out.print("BOOM");
                return;
            }

            Cursor contactCursor = null;
            String contactId = null;
            String contactName = null;

            try {
                contactCursor = getContentResolver().query(contactUri, null, null, null, null);
                if (contactCursor.getCount() == 0) {
                    System.out.print("No Addresses");
                    return;
                }
                if (contactCursor.moveToFirst()) {
                    contactId = contactCursor.getString(contactCursor.getColumnIndex(ContactsContract.Contacts._ID));
                    contactName = contactCursor.getString(contactCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                }
            } catch (IllegalStateException ex) {
                System.out.print("Exception: " + ex.toString());
            } finally {
                if (contactCursor != null && !contactCursor.isClosed()) {
                    contactCursor.close();
                }
            }

            List<Address> addressList = getAddressList(contactId);

            for (Address address : addressList) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this)
                        .setTitle(contactName)
                        .setMessage(address.toString());
                builder.create().show();
            }

        }
    }

    /**
     * @param id
     * @return
     */
    private Uri getContactUri(String id) {
        Uri.Builder builder = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, id).buildUpon();
        builder.appendPath(ContactsContract.Contacts.Entity.CONTENT_DIRECTORY);
        return builder.build();
    }


    private List<Address> getAddressList(String contactId) {

        ArrayList<Address> addressList = new ArrayList<Address>();
        String[] projection = {
                StructuredPostal._ID,
                StructuredPostal.TYPE,
                StructuredPostal.LABEL,
                StructuredPostal.STREET,
                StructuredPostal.POBOX,
                StructuredPostal.NEIGHBORHOOD,
                StructuredPostal.REGION,
                StructuredPostal.POSTCODE,
                StructuredPostal.COUNTRY,
                StructuredPostal.FORMATTED_ADDRESS
        };
        String selection = ContactsContract.Data.MIMETYPE + "='" + StructuredPostal.CONTENT_ITEM_TYPE + "' and " + ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = " + contactId;
        Cursor addressCursor = null;
        Uri contactUri = getContactUri(contactId);

        try {

            addressCursor = getContentResolver().query(contactUri, projection, selection, null, null);

            if (addressCursor.getCount() == 0) {
                System.out.print("No Addresses");
                return null;
            }

            int addressTypeIndex = addressCursor.getColumnIndex(StructuredPostal.TYPE);
            int addressLabelIndex = addressCursor.getColumnIndex(StructuredPostal.LABEL);
            int addressStreetIndex = addressCursor.getColumnIndex(StructuredPostal.STREET);
            int addressPOBIndex = addressCursor.getColumnIndex(StructuredPostal.POBOX);
            int addressNeighIndex = addressCursor.getColumnIndex(StructuredPostal.NEIGHBORHOOD);
            int addressRegionIndex = addressCursor.getColumnIndex(StructuredPostal.REGION);
            int addressPostCodeIndex = addressCursor.getColumnIndex(StructuredPostal.POSTCODE);
            int addressCountryIndex = addressCursor.getColumnIndex(StructuredPostal.COUNTRY);
            int addressFormattedIndex = addressCursor.getColumnIndex(StructuredPostal.FORMATTED_ADDRESS);

            while (addressCursor.moveToNext()) {
                Address address = new Address();
                address.type = addressCursor.getString(addressTypeIndex);
                address.label = addressCursor.getString(addressLabelIndex);
                address.street = addressCursor.getString(addressStreetIndex);
                address.pobox = addressCursor.getString(addressPOBIndex);
                address.neighborhood = addressCursor.getString(addressNeighIndex);
                address.region = addressCursor.getString(addressRegionIndex);
                address.postcode = addressCursor.getString(addressPostCodeIndex);
                address.country = addressCursor.getString(addressCountryIndex);
                address.formatted = addressCursor.getString(addressFormattedIndex);
                addressList.add(address);
            }
        } catch (IllegalStateException ex) {
            System.out.print("Exception: " + ex.toString());
        } finally {
            if (addressCursor != null && !addressCursor.isClosed()) {
                addressCursor.close();
            }
        }
        return addressList;

    }

    /**
     *
     */
    class Address {

        public long id;
        public String type;
        public String label;
        public String street;
        public String pobox;
        public String neighborhood;
        public String region;
        public String postcode;
        public String country;
        public String formatted;

        @Override
        public String toString() {
            return "Address{" +
                    "id=" + id +
                    ", type='" + type + '\'' +
                    ", label='" + label + '\'' +
                    ", street='" + street + '\'' +
                    ", pobox='" + pobox + '\'' +
                    ", neighborhood='" + neighborhood + '\'' +
                    ", region='" + region + '\'' +
                    ", postcode='" + postcode + '\'' +
                    ", country='" + country + '\'' +
                    ", formatted='" + formatted + '\'' +
                    '}';
        }

    }

}

